"""Censys API Test
API Documentation: https://censys.io/api"""

from censys import certificates, ipv4, websites

def fetchdomain(domain='eseses.tk'):
    certs = certificates.CensysCertificates()
    example = certs.view('a762bf68f167f6fbdf2ab00fdefeb8b96f91335ad6b483b482dfd42c179be076')

    f = open('./random_certs.json','w')
    f.write(str(example))
    f.close()

    print(example)

    ips = ipv4.CensysIPv4()
    example = ips.view('185.39.9.34')

    f = open('./random_ips.json','w')
    f.write(str(example))
    f.close()

    print(example)


    
    sites = websites.CensysWebsites()
    example = sites.view('x.gg')

    f = open('./random_websites.json','w')
    f.write(str(example))
    f.close()

    print(example)

fetchdomain()
