"""

Flow
1) Receive user input
2) Parse user input 
3) Pass to appropriate API wrappers 
4) Receive output
5) Parse output

Purpose
1) Take one+ objects and search them in Censys and cross-search them in threatconnect
2) Enrich data with Censys and Threatconnect results
"""
import sys
from censys import certificates, ipv4, websites

def argparser(args):
    #TODO
    return {}
def censysdomain(parsed):
    #TODO
    return {}

def tcdomain(parsed):
    #TODO
    return {}

def censyscert(parsed):
    #TODO
    return {}
def tccert(parsed):
    #TODO
    return {}

def tcip(parsed):
    #TODO
    return {}
def censysip(parsed):
    #TODO
    return {}
    


def main():
    parsed = argparser(sys.argv)
    if parsed['isdomain']:
        if parsed['checkCensys']:
            cencheck = censysdomain(parsed['domain'])
        if parsed['checkTC']:
            tccheck = tcdomain(parsed['domain'])
    elif parsed['isip']:
        if parsed['checkCensys']:
            cencheck = censysip(parsed['ip'])
        if parsed['checkTC']:
            tccheck = tcip(parsed['ip'])
    elif parsed['iscert']:
        if parsed['checkCensys']:
            cencheck = censyscert(parsed['cert'])
        if parsed['checkTC']:
            tccheck = tccert(parsed['cert'])
    
        

